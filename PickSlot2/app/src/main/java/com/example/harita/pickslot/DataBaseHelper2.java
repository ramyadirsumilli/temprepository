package com.example.harita.pickslot;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by harita on 24/05/2016.
 */
public class DataBaseHelper2 extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "DetailsOfPerson.db";
    public static final String TABLE_NAME = "Data_table";
    public static final String COL_1 = "id";
    public static final String COL_2 = "persontype";
    public static final String COL_3 = "firstname";
    public static final String COL_4 = "lastname";
    public static final String COL_5 = "emailid";
    public static final String COL_6 = "password";
    public static final String TABLE_ADD = "AddAppointment_table";
    public static final String COL1 = "id";
    public static final String COL2 = "ToTime";
    public static final String COL3 = "FromTime";
    public static final String COL4 = "Date";
   // public static final String COL_5 = "emailid";
    //public static final String COL_6 = "password";


    public DataBaseHelper2(Context context) {
        super(context, DATABASE_NAME, null,1);
    }

    public DataBaseHelper2(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " +TABLE_NAME +"(id INTEGER PRIMARY KEY AUTOINCREMENT, persontype TEXT, firstname TEXT, lastname TEXT, emailid EMAIL, password PASSWORD)");
        db.execSQL("create table " +TABLE_ADD +"(id INTEGER PRIMARY KEY AUTOINCREMENT, ToTime DATETIME DEFAULT CURRENT_TIME, FromTime DATETIME DEFAULT CURRENT_TIME, Date DATE DEFAULT CURRENT_DATE)");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " +TABLE_NAME);

        onCreate(db);
    }




    public boolean insertData(String firstname, String lastname, String emailid, String password){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentvalue =  new ContentValues();
        contentvalue.put(COL_3,firstname);
        contentvalue.put(COL_4,lastname);
        contentvalue.put(COL_5,emailid);
        contentvalue.put(COL_6, password);

        long result = db.insert(TABLE_NAME,null,contentvalue);
        if (result == -1)return false;
        else return true;

    }
    public Cursor getInformation() {
        SQLiteDatabase db = this.getWritableDatabase();
       // String[] projections = {"firstname","emailid"};
        Cursor cur;
        //cur = db.query(TABLE_NAME, new String[]{"firstname","emailid"},null,null,null,null,null,null);
        cur=db.rawQuery("select * from "+TABLE_NAME,null);
       // cur= db.rawQuery("select * from" + TABLE_NAME,null);
        return  cur;

        //return email;

    }
}
