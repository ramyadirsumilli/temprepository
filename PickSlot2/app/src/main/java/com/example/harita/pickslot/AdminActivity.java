package com.example.harita.pickslot;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.content.Intent;
import android.widget.Toast;

public class AdminActivity extends Activity {
    DataBaseHelper Datadb2;
   // SQLiteDatabase db;
    Cursor cur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.adminpage);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        Datadb2 = new DataBaseHelper(this);
      //  db = Datadb2.getReadableDatabase();

        final AlertDialog alrt = new AlertDialog.Builder(this).create();
       // get_Inform();
    }


    private void setSupportActionBar(Toolbar toolbar) {
    }


    public void OnbuttonClick(View v) {

        if (v.getId() == R.id.addUser_b) {
            Intent i;
            i = new Intent(AdminActivity.this, FirstLayoutActivity.class);
            AdminActivity.this.startActivity(i);
        }
        if (v.getId() == R.id.DeleteButton) {
          cur = Datadb2.getInformation();

          if(cur.getCount()!=0)
            {
                Toast.makeText(AdminActivity.this, "Data Successfully Retrived ", Toast.LENGTH_LONG).show();
                //String email =cur.getString(cur.getColumnIndex("emailid"));
                Intent del = new Intent(this,UserDataListActivity.class);
                startActivity(del);
           }
           else Toast.makeText(AdminActivity.this, "Retrive Failed " , Toast.LENGTH_LONG).show();


           /* Intent del=new Intent(AdminActivity.this,DeleteActivity.class);
            AdminActivity.this.startActivity(del);*/
        }
    }


   public void showMessage(String title, String mess) {
        AlertDialog.Builder builder= new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(mess);
        builder.show();
    }
}