package com.example.harita.pickslot;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.widget.Toast;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by ramya on 5/23/16.
 */
public class DataBaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "person.db";
    public static final String TABLE_NAME = "person_table";
    public static final String COL_1 = "id";
    public static final String COL_2 = "persontype";
    public static final String COL_3 = "firstname";
    public static final String COL_4 = "lastname";
    public static final String COL_5 = "emailid";
    public static final String COL_6 = "password";
    public static final String SLOT_TABLE = "slot_table";
    public static final String SCOL_1 = "id";
    public static final String SCOL_2 = "emailid";
    public static final String SCOL_3 = "password";
    public static final String SCOL_4 = "frmtime";
    public static final String SCOL_5 = "totime";
    public static final String SCOL_6 = "date";
    public static final String SCOL_7 = "room";
    public static final String SCOL_8 = "status";
    public static final String TASK_TABLE = "task_table";
    public static final String TCOL_1 = "id";
    public static final String TCOL_2 = "emailid";
    public static final String TCOL_3 = "task";
    public static final String TCOL_4 = "status";



    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        getEmails();
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME + "(id INTEGER PRIMARY KEY AUTOINCREMENT, persontype TEXT, firstname TEXT, lastname TEXT, emailid EMAIL, password PASSWORD)");
        db.execSQL("create table " +SLOT_TABLE +"(ID INTEGER PRIMARY KEY AUTOINCREMENT,frmtime TEXT,totime TEXT,date TEXT,  emailid EMAIL, password PASSWORD,room TEXT,status TEXT)" );
        db.execSQL("create table " +TASK_TABLE +"(ID INTEGER PRIMARY KEY AUTOINCREMENT, emailid EMAIL, task TEXT,status TEXT)" );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " +SLOT_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " +TASK_TABLE);
        onCreate(db);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public DataBaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }


    public Cursor getInformation(SQLiteDatabase db) {
        String[] projections = {"COL_3","COL_5"};
        Cursor cur = db.query(TABLE_NAME,projections,null,null,null,null,null);

            return  cur;

           }
    public String getSinlgeEntry(String em) {
        SQLiteDatabase db = this.getReadableDatabase();
        String emailcolumn = "emailid= ?";

        //Cursor cursor = db.query(TABLE_NAME,new String[] {"password"},emailcolumn, new String[] {em}, null, null, null);
        Cursor cursor = db.query(TABLE_NAME,null,emailcolumn, new String[] {em}, null, null, null);

        if (cursor.getCount() < 1)
        {
            cursor.close();
            return "NOT EXIST";
        }
        cursor.moveToFirst();
        String password = cursor.getString(cursor.getColumnIndex(COL_6));

        cursor.close();
        return password;
    }

   /* public String getData(Cursor cur) {

        String email = null;
        while (cur.moveToNext()) {
            email = cur.getString(cur.getColumnIndex("emailid"));
        }
        return email;
    }*/

    public boolean insertData(String firstname, String lastname, String emailid, String password){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentvalue =  new ContentValues();
        contentvalue.put(COL_3,firstname);
        contentvalue.put(COL_4,lastname);
        contentvalue.put(COL_5,emailid);
        contentvalue.put(COL_6, password);
        long result = db.insert(TABLE_NAME,null,contentvalue);
        if (result == -1)return false;
        else return true;

    }
    public String getEmails(){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "Select * FROM " + TABLE_NAME ;
        Cursor cursor = db.rawQuery(query, null);
       // String emails= cursor.getString(1);
        String data = new String();
        if (cursor.moveToFirst()){
            do{
                data = cursor.getString(cursor.getColumnIndex("emailid"));
            }while(cursor.moveToNext());
        }
        cursor.close();
        return data;

    }
    public boolean insertDateAppointment (String email, String psw, java.util.Date From, java.util.Date To, java.util.Date date, String Room, String Status) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SCOL_2, email);
        contentValues.put(SCOL_3, psw);
        contentValues.put(SCOL_4, getDateTime(From));
        contentValues.put(SCOL_5,getDateTime(To));
        contentValues.put(SCOL_6, getDateTime(date));
        contentValues.put(SCOL_7 , Room);
        contentValues.put(SCOL_8, Status);
        long result = db.insert(SLOT_TABLE, null, contentValues);
        if (result == 1)
            return false;
        else
            return true;
    }
    public boolean insertDateAppointment (String email, String task){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TCOL_2, email);
        contentValues.put(TCOL_3, task);
        long result = db.insert(TASK_TABLE, null, contentValues);
        if (result == 1)
            return false;
        else
            return true;
    }
    public Cursor getInformation() {
        SQLiteDatabase db = this.getWritableDatabase();
        // String[] projections = {"firstname","emailid"};
        Cursor cur;
        //cur = db.query(TABLE_NAME, new String[]{"firstname","emailid"},null,null,null,null,null,null);
        cur=db.rawQuery("select * from "+TABLE_NAME,null);
        // cur= db.rawQuery("select * from" + TABLE_NAME,null);
        return  cur;

        //return email;

    }
    private String getDateTime(java.util.Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return dateFormat.format(date);
    }
    }

