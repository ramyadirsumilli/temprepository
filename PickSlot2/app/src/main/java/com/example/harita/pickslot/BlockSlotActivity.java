package com.example.harita.pickslot;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.EdgeEffectCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.SimpleTimeZone;


public class BlockSlotActivity extends AppCompatActivity {
    EditText datetext, fromtimetxt, totimetxt, addAppemailtxt, pass;
    Button addAppBut;
    // TimePicker Time;
    DataBaseHelper persondb1;
    Button AddSlot_b;
    Date dateAdd;
    SimpleDateFormat dtfrm;
    private int year, month, day, hur, min;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_block_slot);
        fromtimetxt = (EditText) findViewById(R.id.FromTIme);
        totimetxt = (EditText) findViewById(R.id.ToTime);
        datetext = (EditText) findViewById(R.id.Date);
        addAppBut = (Button) findViewById(R.id.addApp);
        addData();
        persondb1 = new DataBaseHelper(this);
        fromtimetxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == fromtimetxt) {
                    final Calendar frmCal = Calendar.getInstance();
                    final TimePickerDialog.OnTimeSetListener FromTime = new
                            TimePickerDialog.OnTimeSetListener() {

                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                    frmCal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                    frmCal.set(Calendar.MINUTE, minute);
                                    fromtimetxt.setText("" + hourOfDay + ":" + minute);
                                    //updateLabel();
                                }


/*

                                                               private void updateLabel() {

                                                                   // String myFormat = "hh:mm"; //In which you need put here
                                                                   //SimpleTimeZone sdf = new SimpleTimeZone(myFormat, Locale.US);

                                                                   //datetext.setText(sdf.format(myCalendar.getTime()));
                                                               }
*/

                            };
                    fromtimetxt.setOnTouchListener(new View.OnTouchListener() {

                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                                new TimePickerDialog(BlockSlotActivity.this, FromTime, frmCal
                                        .get(Calendar.HOUR_OF_DAY), frmCal.get(Calendar.MINUTE),
                                        true).show();
                            }
                            return true;
                        }
                    });
                }
            }
        });
        totimetxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == totimetxt) {
                    final Calendar ToCal = Calendar.getInstance();

                    final TimePickerDialog.OnTimeSetListener ToTime = new
                            TimePickerDialog.OnTimeSetListener() {

                                @Override
                                public void onTimeSet(TimePicker view, int hourDay, int min) {
                                    ToCal.set(Calendar.HOUR_OF_DAY, hourDay);
                                    ToCal.set(Calendar.HOUR_OF_DAY, min);
                                    totimetxt.setText("" + hourDay + ":" + min);

                                }


                            };
                    totimetxt.setOnTouchListener(new View.OnTouchListener() {

                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                                new TimePickerDialog(BlockSlotActivity.this, ToTime, ToCal
                                        .get(Calendar.HOUR_OF_DAY), ToCal.get(Calendar.MINUTE),
                                        true).show();
                            }
                            return true;
                        }
                    });
                }
            }
        });


        datetext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == datetext) {
                    final Calendar myCalendar = Calendar.getInstance();

                    final DatePickerDialog.OnDateSetListener date = new
                            DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                      int dayOfMonth) {
                                    // TODO Auto-generated method stub
                                    myCalendar.set(Calendar.YEAR, year);
                                    myCalendar.set(Calendar.MONTH, monthOfYear);
                                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                    updateLabel();
                                }

                                private void updateLabel() {

                                    String myFormat = "dd/MM/yy"; //In which you need put here
                                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.GERMANY);

                                    datetext.setText(sdf.format(myCalendar.getTime()));

                                }


                            };
                    datetext.setOnTouchListener(new View.OnTouchListener() {

                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                                new DatePickerDialog(BlockSlotActivity.this, date, myCalendar
                                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                            }
                            return false;
                        }
                    });


                }

            }

        });
    }

    public void addData() {
         String myFormat = "dd/MM/yy"; //In which you need put here
        final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.GERMANY);
        final AlertDialog alrt = new AlertDialog.Builder(this).create();
        addAppBut.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

               // java.sql.Date from = (java.sql.Date) new Date();
                //java.sql.Date to = (java.sql.Date) new Date();
                addAppemailtxt = (EditText) findViewById(R.id.email_address);

                assert addAppemailtxt != null;
                String email = addAppemailtxt.getText().toString();
                pass = (EditText) findViewById(R.id.password);
                assert pass != null;

                String pswd = pass.getText().toString();

                try {
                   dateAdd =  sdf.parse(datetext.getText().toString());
                } catch (ParseException e) {
                  e.printStackTrace();
                }
                // String storedPassword = persondb1.getSinlgeEntry(email);
                //if (pswd.equals(storedPassword)) {
                boolean slot_update = persondb1.insertDateAppointment(email,pswd,null,null,dateAdd,null,null);
                if (slot_update = true)
                    Toast.makeText(BlockSlotActivity.this, "Data Inserted", Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(BlockSlotActivity.this, "Data not Inserted", Toast.LENGTH_LONG).show();

                BlockSlotActivity.this.finish();
                //  if(slot_update = true) Toast.makeText(BlockSlotActivity.this, "inserted" +day , Toast.LENGTH_LONG).show();
                // else Toast.makeText(BlockSlotActivity.this, "Failed to insert slot", Toast.LENGTH_LONG).show();
                //}
                //else Toast.makeText(BlockSlotActivity.this, "Failed to stor password ", Toast.LENGTH_LONG).show();
                //}


            }
        });
        // }

    }
}