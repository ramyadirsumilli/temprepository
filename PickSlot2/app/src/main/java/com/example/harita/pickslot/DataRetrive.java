package com.example.harita.pickslot;

/**
 * Created by harita on 25/05/2016.
 */
public class DataRetrive {
    private String name;
    private String email;
    public DataRetrive(String name,String email)
    {
        this.email=email;
        this.name=name;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
