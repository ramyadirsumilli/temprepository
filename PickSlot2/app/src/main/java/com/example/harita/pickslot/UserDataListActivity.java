package com.example.harita.pickslot;

import android.app.ListActivity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class UserDataListActivity extends AppCompatActivity {
    ListView listView;
    DataBaseHelper Datadb;
    SQLiteDatabase db;
    Cursor cur;
    ListDataAdapter listdata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       setContentView(R.layout.data_list_layout);
        listView = (ListView)findViewById(R.id.list_view);
        listdata = new ListDataAdapter(getApplicationContext(), R.layout.rowlayout);
        listView.setAdapter(listdata);
        Datadb = new DataBaseHelper(getApplicationContext());
        db = Datadb.getWritableDatabase();
         cur = Datadb.getInformation();
        if (cur.moveToFirst()) {
            do {
                String name, email;
                name = cur.getString(2);
                email = cur.getString(4);
                DataRetrive retrive = new DataRetrive(name, email);
                listdata.add(retrive);
            } while (cur.moveToNext());
        }
    }


     /*  String[] values;
        values = new String[]{ "Android", "iPhone", "WindowsMobile",
                "Blackberry", "WebOS", "Ubuntu", "Windows7", "Max OS X",
                "Linux", "OS/2" };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,values);
        setListAdapter(adapter);

    }
    protected void onListItemClick(ListView l, View v, int position, long id) {
        String item = (String) getListAdapter().getItem(position);
        Toast.makeText(this, item + " selected", Toast.LENGTH_LONG).show();
    }*/

        }
