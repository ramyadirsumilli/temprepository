package com.example.harita.pickslot;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harita on 25/05/2016.
 */
public class ListDataAdapter extends ArrayAdapter {
    List<Object> list=new ArrayList<Object>();
    public ListDataAdapter(Context context, int resource) {
        super(context, resource);
    }
static class LayoutHandler
{
    TextView Name;
    TextView Email;
}
    @Override
    public void add(Object object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row=convertView;
        LayoutHandler layoutHandler;
        if(row==null)
        {
            LayoutInflater layoutInflater=(LayoutInflater)(this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE));
            row=layoutInflater.inflate(R.layout.rowlayout,parent,false);
            layoutHandler=new LayoutHandler();
            layoutHandler.Name=(TextView)row.findViewById(R.id.Fname);
            layoutHandler.Email=(TextView)row.findViewById(R.id.email);
            row.setTag(layoutHandler);
        }
        else layoutHandler = (LayoutHandler) row.getTag();

            DataRetrive dataRetrive=(DataRetrive) this.getItem(position);
        layoutHandler.Name.setText(dataRetrive.getName());
        layoutHandler.Email.setText(dataRetrive.getEmail());

        return row;
        //return super.getView(position, convertView, parent);
    }
}
